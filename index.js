var express = require('express');
var routes = require('./routers/routes');
var job_routes = require('./routers/JobsRoutes');
var bodyParser = require('body-parser')

var mongoose = require("mongoose");

mongoose.connect('mongodb://localhost:27017/api')
var db = mongoose.connection

var app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
// parse application/x-www-form-urlencoded 
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json 
app.use(bodyParser.json())

// Users
app.get('/users', routes.getUsers)
app.get('/users/user/:firstname', routes.getUsersByName)
app.post('/users', routes.addUser)
app.put('/users/:id', routes.updateUser)

// Jobs
app.get('/jobs', job_routes.getJobs)
app.get('/jobs/search/:jobType', job_routes.getJobByType)
app.post('/jobs', job_routes.addJob)
app.put('/jobs/:id', job_routes.updateJob)

app.listen(1027);
console.log('starting...');