var usersModel = require('../myModels/usersModel')

		module.exports.getUsers = function(callback){
		usersModel.find(function(err, docs){
			if(err){
				callback({data:null, success:false})
			}else{
				callback({docs, success:true})
				console.log(docs)
			};
		});	
	};
	module.exports.getUserByName = function(firstname, callback){
		console.log(firstname)
		usersModel.find({firstname: firstname}, function(err, docs){
			if(err){
				console.log(err)
				callback({'Error': + err})
			}else{
				callback({'Success': JSON.stringify(docs)})
			}
		})
	}
	module.exports.addUser = function(data, callback){

		usersModel.create(data, function(err, result){
			if(err){
				callback({'Error': 'An error occured'})
			}else{
                callback('Success: ' + JSON.stringify(result));
            }
		})
	}

	module.exports.updateUser = function(id, data, callback){

		usersModel.update({_id:id}, data, function(err, status){
			if(data){
                callback({status:'Success ' + JSON.stringify(data)});
			}else{
				callback({'Error': 'Something went wrong'})
			}
		})
	}
