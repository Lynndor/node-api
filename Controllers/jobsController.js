var jobsModel = require('../myModels/jobsModel')

module.exports.getJobs = function(callback){
	jobsModel.find(function(err, data){
		if(err){
			callback({'Error': err, success:false})
		}
		else{
			callback({Jobs: data, success:true})
		}
	})
}

module.exports.addJob = function(newjob, callback){
	jobsModel.create(newjob, function(err, result){
		if(err){
			callback({data:null, success:false})
		}else{
			callback({created:result, success:true})
		}
	})
}
module.exports.updateJob = function(id, job_update, callback){
	jobsModel.update({_id:id}, job_update, function(err, result){
		if(err){
			callback({data: null, Error: err, success:false})
		}else{
			callback({data:result, success: true})
		}
	})
}
module.exports.getJobByType = function(jobType, callback){
	console.log(jobType)
	jobsModel.find({jobType:jobType}, function(err, results){
		if(err){
			callback({data: null, Error: err, success:false})
		}else{
			callback({data: results, success: true})
		}
	})
}