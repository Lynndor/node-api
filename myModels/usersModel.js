var mongoose = require('mongoose');
var Schema = mongoose.Schema

var userSchema = new Schema({
	firstname:{
		type: String,
		required: true
	}, 
	lastname:{
		type: String,
		required: true
	},
	cellnumber:{
		type: Number,
		required: true
	}
})

// var jobSchema = new Schema({
// 	jobType:{
// 		type: String,
		
// 	},
// 	jobName:{
// 		type: String,
		
// 	},
// 	jobDescription:{
// 		type: String,
		
// 		max: 2000
// 	},
// 	budget:{
// 		type: Number,
// 		required: true,
// 	},
// 	createdBy:{
// 		id:{
// 			type: Schema.Types.ObjectId,
// 			ref: 'userSchema',
			
// 		},
// 		name:{
// 			type: String,
			
// 		}
// 	},
// 	createdDate:{
// 		type: Date,
// 		default: Date.now,
		
// 	},
// 	updatedDate:{
// 		type: Date,
// 		default: Date.now,
		
// 	},
// 	comments:[{
// 		text:{
// 			type: String,
// 			trim: true,
// 			max:3000
// 		},
// 		author: {
// 			id:{
// 				type: Schema.Types.ObjectId,
// 				ref: 'userSchema'
// 			},
// 			name: String
// 		}
// 	}]

// })

// jobSchema.pre('save', function(next){
// 	if(!this.isModified('updatedDate')) this.updatedDate = new Date;
// 	next();
// })

module.exports = mongoose.model("User", userSchema);
// module.exports = mongoose.model("Job", jobSchema)