var jobscontroller = require('../Controllers/jobsController')

exports.getJobs = function(req, res){
	jobscontroller.getJobs(function(err, data){
		if(data == true){
			res.send(data);
		}
		else{
			res.send(err)
		}
	})
}
exports.getJobByType = function(req, res){
	var jobType = req.params.jobType;
	console.log(jobType)
	jobscontroller.getJobByType(jobType, function(err, jobs){
		if(err){
			res.send(err)
		}else{
			res.send('Found:' + JSON.stringify(jobs))
		}

	})
}

exports.addJob= function(req, res){
	var newJob = req.body;

	// res.send('Adding new Job: ' + JSON.stringify(newJob));	
	jobscontroller.addJob(newJob, function(err, data){
		if(err){
			res.send(err)
		}else{
			res.send('Job added: ' + JSON.stringify(data))

		}
	})
}

exports.updateJob = function(req, res){
	var job_update = req.body;
	var id = req.params.id;

	jobscontroller.updateJob(id, job_update, function(err, data){
		if(err){
			res.send(err)
		}else{
			res.send('updated successful:'+ JSON.stringify(data))
		}
	})
}