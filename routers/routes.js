var model = require('../Controllers/usersController');


exports.getUsers = function(req, res){
	model.getUsers(function(err, data){
		if(data == true){
			res.send(data);
		}
		else{
			res.send(err)
		}
	})
}
exports.getUsersByName = function(req, res){
	var firstname = req.params.firstname;
	console.log(firstname)

	model.getUserByName(firstname, function(err, data){
		if(err){	
			res.send(err)
		}else{
			res.send('Found user:'+JSON.stringify(data))
		}
	})
}

exports.addUser = function(req, res){
	var userInfo = req.body;

	res.send('Adding user: ' + JSON.stringify(userInfo));
	model.addUser(userInfo, function(err, data){
		if(err){
			res.send(err)
			console.log(err)
		}else{
			res.send('Added user: ' + JSON.stringify(data))
		}
	})
}

exports.updateUser = function(req, res){
	var id = req.params.id;
	var updateInfo =  req.body;
	
	model.updateUser(id, updateInfo, function(err, data){
		if(err){
			res.send(err);
		}else{
			res.send('Updated user: ' + JSON.stringify(data))
		}
	})
}